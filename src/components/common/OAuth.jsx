import React from "react";
import { AiFillGoogleCircle } from "react-icons/ai";

import { GoogleAuthProvider, getAuth, signInWithPopup } from "firebase/auth";

import { db } from "../../firebase";
import { doc, serverTimestamp, setDoc, getDoc } from "@firebase/firestore";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { react } from "@babel/types";
export default function OAuth() {
  const navigate = useNavigate();

  async function onGgAuth() {
    try {
      const auth = getAuth();
      const provider = new GoogleAuthProvider();
      const result = await signInWithPopup(auth, provider);
      const user = result.user;

      const docRef = doc(db, "users", user.uid);
      const docSnap = await getDoc(docRef);
      if (!docSnap.exists()) {
        await setDoc(docRef, {
          name: user.displayName,
          email: user.email,
          timestamp: serverTimestamp(),
        });
      }

      toast.success(`${user.displayName} was Logged in`);
      navigate("/");
    } catch (error) {
      toast.error("Could not sign with google");
    }
  }
  return (
    <button
      type="button"
      onClick={onGgAuth}
      className="flex justify-center item-center w-full bg-red-600 text-white px-7 
      py-3 rounded-md text-sm font-medium uppercase shadow-md hover:bg-red-700 transition duration-200 ease-in-out hover:shadow-lg active:bg-red-800"
    >
      <AiFillGoogleCircle className="text-2xl bgwhite rounded-full mr-2" />
      Sign in with google
    </button>
  );
}
