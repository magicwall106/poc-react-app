// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics"
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAiDBluAV6gMMdyZ6nA9M9A10fHPL_qGbQ",
  authDomain: "webapp-poc-aebe5.firebaseapp.com",
  projectId: "webapp-poc-aebe5",
  storageBucket: "webapp-poc-aebe5.appspot.com",
  messagingSenderId: "1071789609527",
  appId: "1:1071789609527:web:59828fabf9bb52ab89a5b2",
  measurementId: "G-G33BK2Z584",
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const analytics = getAnalytics();
export const db = getFirestore();
